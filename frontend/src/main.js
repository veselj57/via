import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'


import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import NewMeme from "@/components/NewMeme";
import RandomMeme from "@/components/RandomMeme";

Vue.config.productionTip = false

Vue.use(VueRouter)


const routes = [
  { path: '/random', component: RandomMeme },
  { path: '/new', component: NewMeme }
]


const router = new VueRouter({
  routes // short for `routes: routes`
})


new Vue({
  render: h => h(App),
  router
}).$mount('#app')
