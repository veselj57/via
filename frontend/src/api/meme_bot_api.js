
export default {
    types: [
        {name: "Jednořádkový", value: "SINGLE"},
        {name: "Dvouřádkový", value: "TWOPART"}
    ],
    images: [
        {name: "Laughing Goat", value: "Laughing-Goat"},
        {name: "Bad Luck Brian", value: "Bad-Luck-Brian"},
        {name: "Advice Peeta", value: "Advice-Peeta"},
    ],
    topics: [
        {name: "Programování", value: "Programming"},
        {name: "Černý humor", value: "Dark"},
        {name: "Strašidelný", value: "Spooky"},
        {name: "Vánoce", value: "Christmas"},
        {name: "Náhodný", value: "Any"},
        {name: "Smíšený", value: "Miscellaneous"},
        {name: "Slovní hříčka", value: "Pun"},
    ]

}