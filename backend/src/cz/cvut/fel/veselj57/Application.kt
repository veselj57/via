package cz.cvut.fel.veselj57

import cz.cvut.fel.veselj57.api.RandomMemeAPI
import cz.cvut.fel.veselj57.services.joke.Joke
import cz.cvut.fel.veselj57.services.joke.JokeAPI
import cz.cvut.fel.veselj57.services.meme_generator.MemeGeneratorAPI
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.features.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.auth.*
import io.ktor.client.*
import kotlinx.serialization.json.Json
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.ktor.kodein


val strign = """{
    "error": false,
    "category": "Dark",
    "type": "twopart",
    "setup": "What do you call a kid with no arms and an eyepatch?",
    "delivery": "Names.",
    "flags": {
        "nsfw": false,
        "religious": false,
        "political": false,
        "racist": false,
        "sexist": false
    },
    "id": 121,
    "lang": "en"
}"""


/*fun main(){
    val json = Json { ignoreUnknownKeys = true }
    print(json.decodeFromString(Joke.serializer(), strign))
}*/


fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        header("MyCustomHeader")
        allowCredentials = true
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }

    install(Authentication) {

    }

    kodein {
        bind<JokeAPI>() with singleton { JokeAPI() }
        bind<MemeGeneratorAPI>() with singleton { MemeGeneratorAPI() }
    }

    RandomMemeAPI()
}

