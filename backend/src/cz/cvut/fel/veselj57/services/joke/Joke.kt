package cz.cvut.fel.veselj57.services.joke

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
sealed class Joke{

    @Serializable
    @SerialName("twopart")
    data class TwoPartJoke(
        val setup: String,
        val delivery: String

    ): Joke()

    @Serializable
    @SerialName("single")
    data class SinglePartJoke(
        val joke: String
    ): Joke()


}