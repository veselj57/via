package cz.cvut.fel.veselj57.services.joke

enum class JokeTopic {
    Programming, Miscellaneous, Dark, Pun, Spooky, Christmas, Any
}