package cz.cvut.fel.veselj57.services.joke

import io.ktor.application.*
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.get
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.*
import io.ktor.util.url
import io.netty.util.NettyRuntime


class JokeAPI(val base: String = "https://sv443.net/jokeapi/v2") {

    val jsonConfig = kotlinx.serialization.json.Json { ignoreUnknownKeys = true }


    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer(jsonConfig)
        }

        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.ALL
        }


    }



    suspend fun getRandomJoke(topic: JokeTopic = JokeTopic.Any, type: JokeType?): Joke {

        val response = client.get<String> {
            url("$base/joke/${topic.name}?")

            if (type != null)
                parameter("type", type)

            contentType(ContentType.Application.Json)
        }

        return try {
            jsonConfig.decodeFromString(Joke.serializer(), response)
        }catch (e: Exception){
            Joke.SinglePartJoke("404 - Joke not found")
        }
    }

}