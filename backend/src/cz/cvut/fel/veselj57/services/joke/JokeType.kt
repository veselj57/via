package cz.cvut.fel.veselj57.services.joke

enum class JokeType {
    TWOPART, SINGLE
}