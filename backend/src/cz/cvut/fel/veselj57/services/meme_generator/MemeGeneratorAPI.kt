package cz.cvut.fel.veselj57.services.meme_generator

import cz.cvut.fel.veselj57.services.joke.Joke
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*

class MemeGeneratorAPI(val base: String = "http://apimeme.com"){

    val client = HttpClient(CIO)

    suspend fun generateMeme(joke: Joke, meme: String) = client.get<ByteArray>(){
        url("$base/meme")

        parameter("meme", meme)

        when(joke){
           is Joke.TwoPartJoke -> {
               parameter("top", joke.setup)
               parameter("bottom", joke.delivery)
           }
           is Joke.SinglePartJoke -> {
               parameter("top", "")
               parameter("bottom", joke.joke)
           }
        }
    }

}