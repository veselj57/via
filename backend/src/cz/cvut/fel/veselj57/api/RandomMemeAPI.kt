package cz.cvut.fel.veselj57.api

import cz.cvut.fel.veselj57.services.joke.Joke
import cz.cvut.fel.veselj57.services.joke.JokeAPI
import cz.cvut.fel.veselj57.services.joke.JokeTopic
import cz.cvut.fel.veselj57.services.joke.JokeType
import cz.cvut.fel.veselj57.services.meme_generator.MemeGeneratorAPI
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import org.kodein.di.generic.instance
import org.kodein.di.ktor.kodein


fun Application.RandomMemeAPI(){

    routing {
        get("/random") {
            val jokeAPI by kodein().instance<JokeAPI>()
            val generatorAPI by kodein().instance<MemeGeneratorAPI>()

            val type = try {
                JokeType.valueOf(call.parameters["type"]!!)
            }catch (e: Exception){
                throw BadRequestException("Missing or not  correctly formatted type parameter")
            }

            val topic = try {
               JokeTopic.valueOf(call.parameters["topic"]!!)
            }catch (e: Exception){
                throw BadRequestException("Missing or not  correctly formatted topic parameter")
            }

            val meme = try {
                call.parameters["image"]!!
            }catch (e: Exception){
                throw BadRequestException("Missing or not  correctly formatted image parameter")
            }

            val joke = jokeAPI.getRandomJoke(topic, type)

            val image = generatorAPI.generateMeme(joke, meme)

            call.respondBytes(image, ContentType.Image.JPEG)
        }


        get("/generate") {
            val generatorAPI by kodein().instance<MemeGeneratorAPI>()

            val top = call.parameters["top_text"]
            val bottom = call.parameters["bottom_text"]
            val meme = call.parameters["image"]

            if (meme == null)
                throw BadRequestException("Must specify Image")

            if (top == null && bottom == null)
                throw BadRequestException("Top or bottom text must be specified")


            val joke = if (top != null && bottom != null)
                Joke.TwoPartJoke(top, bottom)
            else
                Joke.SinglePartJoke(bottom ?: top ?: "")

            val image = generatorAPI.generateMeme(joke, meme)

            call.respondBytes(image, ContentType.Image.JPEG)
        }

    }

}